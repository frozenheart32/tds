﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "../ActorComponents/TDSHealthComponent.h"
#include "../Interface/TDS_IGameActor.h"

bool UTDS_StateEffect::InitObject(AActor* Actor, FName HitBoneName)
{
	if(!Actor) return false;
	
	MyActor = Actor;

	ITDS_IGameActor* MyInterface = Cast<ITDS_IGameActor>(MyActor);
	if(MyInterface)
	{
		MyInterface->AddEffect(this);
	}
	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* MyInterface = Cast<ITDS_IGameActor>(MyActor);
	if(MyInterface)
	{
		MyInterface->RemoveEffect(this);
	}
	
	MyActor = nullptr;
	
	if(this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

//-------------------------------------------------------

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName HitBoneName)
{
	auto Result = Super::InitObject(Actor, HitBoneName);
	if(Result)
	{
		ExecuteOnce();
	}
	return Result;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	//Execute
	if(MyActor)
	{
		auto HealthComponent =
			Cast<UTDSHealthComponent>(MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if(HealthComponent)
		{
			HealthComponent->ChangeHealthValue(Power);
		}
	}
	//Destroy
	DestroyObject();
}



//--------------------------------------------------------------------------------------------------------------


bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName HitBoneName)
{
	bool Result = Super::InitObject(Actor, HitBoneName);

	if(!Result)
	{
		return Result;
	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this,
		&UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this,
		&UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if(ParticleEffect)
	{
		//TODO: Random Init effect with aviable array
		
		
		FVector Location{0};
		auto SceneComponent = Cast<USkeletalMeshComponent>(MyActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if(SceneComponent)
		{
			ParticleEmmiter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, SceneComponent,
			HitBoneName, Location,FRotator::ZeroRotator, FVector{1},
			EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmmiter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MyActor->GetRootComponent(),
			NAME_None, Location,FRotator::ZeroRotator, FVector{1},
			EAttachLocation::SnapToTarget, false);
		}
	}
	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	if(ParticleEmmiter)
	{
		ParticleEmmiter->DestroyComponent();
		ParticleEmmiter = nullptr;
	}
	
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	//Execute
	if(MyActor)
	{
		auto HealthComponent =
			Cast<UTDSHealthComponent>(MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if(HealthComponent)
		{
			HealthComponent->ChangeHealthValue(Power);
		}
	}
}


//-----------------------------------------------------------------------------------------------------

void UTDS_StateEffect_ExecuteTimer_AuraZone::Execute()
{
	if(MyActor && TargetFilterClass && TraceObjectTypes.Num() > 0)
	{
		TArray<AActor*> Targets;
		
		TArray<AActor*> IgnoreActors;
		IgnoreActors.Init(MyActor, 1);
		
		FVector Position = MyActor->GetActorLocation();
		auto Result = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Position, RadiusOfZone, TraceObjectTypes,
			TargetFilterClass, IgnoreActors, Targets);

		if(Result)
		{
			for (auto Target : Targets)
			{
				auto HealthComponent =
					Cast<UTDSHealthComponent>(Target->GetComponentByClass(UTDSHealthComponent::StaticClass()));
				if(HealthComponent)
				{
					HealthComponent->ChangeHealthValue(Power);
				}
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UTDS_StateEffect_ExecuteTimer_OverlappedZone::Execute. Is Invalid variables"));
	}
}


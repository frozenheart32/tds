﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "TDS_StateEffect.generated.h"


UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDS_StateEffect : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurfaces;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings")
	bool IsStackable;
	
	AActor* MyActor = nullptr;
	
	virtual bool InitObject(AActor* Actor, FName HitBoneName);
	virtual void DestroyObject();
};


UCLASS()
class TDS_API UTDS_StateEffect_ExecuteOnce : public UTDS_StateEffect
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings Execute Once")
	float Power = 20.f;
	
	bool InitObject(AActor* Actor, FName HitBoneName) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();
};

UCLASS()
class TDS_API UTDS_StateEffect_ExecuteTimer : public UTDS_StateEffect
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings Execute Once")
	float Power = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings Execute Once")
	float Timer = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings Execute Once")
	float RateTime = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings Execute Once")
	UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmmiter = nullptr;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
	
	bool InitObject(AActor* Actor, FName HitBoneName) override;
	void DestroyObject() override;

	virtual void Execute();
};


UCLASS()
class TDS_API UTDS_StateEffect_ExecuteTimer_AuraZone : public UTDS_StateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Overlapped Zone Settings")
	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Overlapped Zone Settings")
	TSubclassOf<AActor> TargetFilterClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Overlapped Zone Settings")
	float RadiusOfZone = 20.f;
	
	virtual void Execute() override;
};
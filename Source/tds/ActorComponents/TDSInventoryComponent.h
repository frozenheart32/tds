﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../FuncLibrary/Types.h"
#include "TDSInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionInfo, int32, NewCurrentWeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChanged, EWeaponType, AmmoType, int32, Count);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChanged, int32, SlotIndex, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAviable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, UpdatedWeaponSlotIndex, FWeaponSlot, NewWeaponSlot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, WeaponSlotIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, WeaponSlotIndex);



UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTDSInventoryComponent();

	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnSwitchWeapon OnSwitchWeapon;
	
	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnAmmoChanged OnAmmoChanged;
	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnWeaponAdditionalInfoChanged OnWeaponAdditionalInfoChanged;
	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnWeaponAmmoAviable OnWeaponAmmoAviable;
	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnUpdateWeaponSlots OnUpdateWeaponSlots;
	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnWeaponHaveRound OnWeaponHaveRound;
	UPROPERTY(BlueprintAssignable, Category="Inventory")
	FOnWeaponNotHaveRound OnWeaponNotHaveRound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Weapons")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Weapons")
	TArray<FAmmoSlot> AmmoSlots;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Weapons")
	int32 MaxSlotWeapon = 0;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
	bool SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo,
	                                           bool bIsForward);

	bool SwitchWeaponByIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo);

	FAdditionalWeaponInfo GetAdditionalWeaponInfo(int32 WeaponIndex);
	int32 GetWeaponSlotIndexByName(FName WeaponNameId);
	bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType);
	FName GetWeaponNameBySlotIndex(int32 SlotIndex);
	void SetAdditionalInfoWeapon(int32 WeaponIndex, FAdditionalWeaponInfo NewInfo);
	
	UFUNCTION(BlueprintCallable)
	void AmmoSlotChangeValue(EWeaponType Type, int32 AmmoChangeCount);
	
	bool CheckAmmoForWeapon(EWeaponType Type, int32& AviableAmmoForWeapon);

	UFUNCTION(BlueprintCallable, Category="Inventory")
	bool CheckCanTakeAmmo(EWeaponType AmmoType);
	UFUNCTION(BlueprintCallable, Category="Inventory")
	bool CheckCanTakeWeapon(int32& FreeSlot);
	UFUNCTION(BlueprintCallable, Category="Inventory")
	bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 SlotIndex, int32 ActiveSlotIndex, FDropItem& DropItem);
	UFUNCTION(BlueprintCallable, Category="Inventory")
	bool TryGetWeaponToInventory(FWeaponSlot NewWeaponSlot);
	UFUNCTION(BlueprintCallable, Category="Inventory")
	bool GetDropItemInfoFromInventory(int32 SlotIndex, FDropItem& DropItem);

	UFUNCTION(BlueprintCallable, Category="Inventory")
	void DropWeaponByIndex(int32 ByIndex, FDropItem& DropItemInfo);

	UFUNCTION(BlueprintCallable, Category="Inventory")
	void InitInventory(TArray<FWeaponSlot> NewWeaponSlots, TArray<FAmmoSlot> NewAmmoSlots);

	UFUNCTION(BlueprintCallable, Category="Inventory")
	TArray<FWeaponSlot> GetWeaponSlots() const;
	UFUNCTION(BlueprintCallable, Category="Inventory")
	TArray<FAmmoSlot> GetAmmoSlots() const;
};

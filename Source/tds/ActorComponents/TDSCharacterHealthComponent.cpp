﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharacterHealthComponent.h"

#include "TimerManager.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UTDSCharacterHealthComponent::UTDSCharacterHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	// ...
}

void UTDSCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if(IsDead) return;
	
	if(ChangeValue < 0.f && Shield > 0.f)
	{
		ChangeShieldValue(ChangeValue);
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDSCharacterHealthComponent::GetCurrentShield() const
{
	return Shield;
}

void UTDSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * DamageCoef;
	
	float DeltaShield = Shield + CurrentDamage;
	float AbsorbedDamage = 0.f;
	if(DeltaShield > 0.f)
	{
		Shield = DeltaShield;
		AbsorbedDamage = CurrentDamage;
		CurrentDamage = 0.f;
	}
	else
	{
		Shield = 0.f;
		AbsorbedDamage = CurrentDamage - DeltaShield;
		CurrentDamage = DeltaShield;
		//Call event fx shield destroy
	}
	
	OnShieldChange.Broadcast(Shield, AbsorbedDamage);
	if(GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryTimer);
		
		GetWorld()->GetTimerManager()
			.SetTimer(TimerHandle_CooldownShieldTimer, this,
				&UTDSCharacterHealthComponent::CooldownShieldEnd, CooldownShieldRecoveryTime, false);
	}
	
	Super::ChangeHealthValue(CurrentDamage);
}

void UTDSCharacterHealthComponent::CooldownShieldEnd()
{
	if(GetWorld())
	{
		GetWorld()->GetTimerManager()
		          .SetTimer(TimerHandle_ShieldRecoveryTimer, this,
		                    &UTDSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecovery;
	
	if(tmp > 100.f)
	{
		Shield = 100.f;
		if(GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecovery);
}
﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSHealthComponent.h"


// Sets default values for this component's properties
UTDSHealthComponent::UTDSHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	//...
}

float UTDSHealthComponent::GetCurrentHealth() const
{
	return Health;
}

void UTDSHealthComponent::SetCurrentHealth(float NewHealthValue)
{
	Health = NewHealthValue;
}

void UTDSHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if(IsDead || ChangeValue == 0.f) return;
	
	if(ChangeValue < 0.f)
	{
		ChangeValue = ChangeValue * DamageCoef;
	}
	
	Health += ChangeValue;
	if(Health > 100.f)
	{
		Health = 100.f;		
	}
	
	OnHealthChange.Broadcast(Health, ChangeValue);

	if(Health <= 0.f)
	{
		IsDead = true;
		OnDead.Broadcast();
	}
}

bool UTDSHealthComponent::CheckDeadStatus() const
{
	return IsDead;
}

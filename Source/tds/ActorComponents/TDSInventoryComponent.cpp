﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventoryComponent.h"

#include "../Game/TDSGameInstance.h"
#include "../Interface/TDS_IGameActor.h"


// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                           FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTDSInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num()-1)
		CorrectIndex = 0;	
	else
		if(ChangeToIndex < 0)
			CorrectIndex = WeaponSlots.Num()-1;

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
			{
				//good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//check ammoSlots for this weapon
					FWeaponInfo myInfo;
					myGI->TryGetWeaponInfoByName(myInfo, WeaponSlots[CorrectIndex].NameItem);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}
	if (!bIsSuccess)
	{		
		int8 iteration = 0;
		int8 Seconditeration = 0;
		int8 tmpIndex = 0;
		while (iteration < WeaponSlots.Num() && !bIsSuccess)
		{
			iteration++;

			if (bIsForward)
			{
				//Seconditeration = 0;

				tmpIndex = ChangeToIndex + iteration;
			}
			else
			{
				Seconditeration = WeaponSlots.Num() - 1;

				tmpIndex = ChangeToIndex - iteration;
			}

			if (WeaponSlots.IsValidIndex(tmpIndex))
			{
				if (!WeaponSlots[tmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
					{
						//WeaponGood
						bIsSuccess = true;
						NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
						NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
						NewCurrentIndex = tmpIndex;
					}
					else
					{
						FWeaponInfo myInfo;
						UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

						myGI->TryGetWeaponInfoByName( myInfo, WeaponSlots[tmpIndex].NameItem);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
								NewCurrentIndex = tmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else
			{
				//go to end of LEFT of array weapon slots
				if (OldIndex != Seconditeration)
				{
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
								NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
								NewCurrentIndex = Seconditeration;
							}
							else
							{
								FWeaponInfo myInfo;
								UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

								myGI->TryGetWeaponInfoByName(myInfo, WeaponSlots[Seconditeration].NameItem);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
										NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
										NewCurrentIndex = Seconditeration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					//go to same weapon when start
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								//WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo myInfo;
								UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

								myGI->TryGetWeaponInfoByName( myInfo, WeaponSlots[Seconditeration].NameItem);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
									{
										if (AmmoSlots[j].Count > 0)
										{
											//WeaponGood, it same weapon do nothing
										}
										else
										{
											//Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					Seconditeration++;
				}
				else
				{
					Seconditeration--;
				}
				
			}
		}
	}	
	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex,OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}		

	return bIsSuccess;
}

bool UTDSInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponInfo PreviosWeaponInfo)
{
	bool bIsSuccess = false;
	FName ToSwitchIdWeapon;
	FAdditionalWeaponInfo ToSwitchAdditionalInfo;

	ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
	ToSwitchAdditionalInfo = GetAdditionalWeaponInfo(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAdditionalInfoWeapon(PreviosIndex, PreviosWeaponInfo);
		OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

		//check ammo slot for event to player		
		EWeaponType ToSwitchWeaponType;
		if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int32 AviableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
			{
				
			}								
		}		
		bIsSuccess = true;
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UTDSInventoryComponent::GetAdditionalWeaponInfo(int32 WeaponIndex)
{
	FAdditionalWeaponInfo Result;
	if(WeaponSlots.IsValidIndex(WeaponIndex))
	{
		bool IsFind = false;
		int32 i = 0;
		while (i < WeaponSlots.Num() && !IsFind)
		{
			if(i == WeaponIndex)
			{
				Result =  WeaponSlots[i].AdditionalInfo;
				IsFind = true;
			}
			i++;
		}

		if(!IsFind)
		{
			UE_LOG(LogTemp, Warning,
				TEXT("UTDSInventoryComponent::GetAdditionalWeaponInfo - No found weapon with index -%d"), WeaponIndex);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning,
				TEXT("UTDSInventoryComponent::GetAdditionalWeaponInfo - Not correct weapon index - %d"), WeaponIndex);
	}

	return Result;
}

int32 UTDSInventoryComponent::GetWeaponSlotIndexByName(FName WeaponNameId)
{
	for (auto i = 0; i< WeaponSlots.Num(); i++)
	{
		const auto& WeaponSlot = WeaponSlots[i];
		if(WeaponSlot.NameItem.IsEqual(WeaponNameId))
		{
			return i;
		}
	}
	return -1;
}

bool UTDSInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType &WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RiffleType;
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		bIsFind = myGI->TryGetWeaponInfoByName(OutInfo,IdWeaponName);
		WeaponType = OutInfo.WeaponType;
	}
	return bIsFind;
}

FName UTDSInventoryComponent::GetWeaponNameBySlotIndex(int32 SlotIndex)
{
	FName Name{};
	if(WeaponSlots.IsValidIndex(SlotIndex))
	{
		Name = WeaponSlots[SlotIndex].NameItem;
	}
	
	return Name;
}

void UTDSInventoryComponent::SetAdditionalInfoWeapon(int32 WeaponIndex, FAdditionalWeaponInfo NewInfo)
{
	if(WeaponSlots.IsValidIndex(WeaponIndex))
	{
		bool IsFind = false;
		int32 i = 0;
		while (i < WeaponSlots.Num() && !IsFind)
		{
			if(i == WeaponIndex)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				IsFind = true;

				OnWeaponAdditionalInfoChanged.Broadcast(WeaponIndex, NewInfo);
			}
			i++;
		}

		if(!IsFind)
		{
			UE_LOG(LogTemp, Warning,
				TEXT("UTDSInventoryComponent::SetAdditionalWeaponInfo - No found weapon with index -%d"), WeaponIndex);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning,
				TEXT("UTDSInventoryComponent::SetAdditionalWeaponInfo - Not correct weapon index - %d"), WeaponIndex);
	}
}

void UTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType Type, int32 AmmoChangeCount)
{
	bool IsFind = false;
	int32 i = 0;
	EWeaponType ResultType = EWeaponType::ShotGunType;
	int32 ResultCount = 0;
	
	while (i < AmmoSlots.Num() && !IsFind)
	{
		auto& Slot = AmmoSlots[i];
		if(Slot.WeaponType == Type)
		{
			Slot.Count += AmmoChangeCount;
			if(Slot.Count > Slot.MaxCount)
			{
				Slot.Count = Slot.MaxCount;
			}
			else if(Slot.Count < 0)
			{
				
			}
			ResultType = Type;
			ResultCount = Slot.Count;
			IsFind = true;
		}
		i++;
	}

	if(IsFind)
	{
		OnAmmoChanged.Broadcast(ResultType, ResultCount);
	}
}

bool UTDSInventoryComponent::CheckAmmoForWeapon(EWeaponType Type, int32& AviableAmmoForWeapon)
{
	AviableAmmoForWeapon = 0;
	for (const auto& AmmoSlot : AmmoSlots)
	{
		if(AmmoSlot.WeaponType == Type)
		{
			AviableAmmoForWeapon = AmmoSlot.Count;
			bool Result = AmmoSlot.Count > 0;
			if(!Result)
				OnWeaponAmmoEmpty.Broadcast(Type);
			
			return  Result;
		}
	}

	if(AviableAmmoForWeapon <= 0)
	{
		OnWeaponAmmoEmpty.Broadcast(Type);
	}
	else
	{
		OnWeaponAmmoAviable.Broadcast(Type);
	}
	
	return false;
}

bool UTDSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	for (const auto& AmmoSlot : AmmoSlots)
	{
		if(AmmoSlot.WeaponType == AmmoType)
		{
			return AmmoSlot.Count < AmmoSlot.MaxCount;
		}
	}
	
	return false;
}

bool UTDSInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
	for (int i=0; i < WeaponSlots.Num(); i++)
	{
		if(WeaponSlots[i].NameItem.IsNone())
		{
			FreeSlot = i;
			return true;
		}
	} 
	return false;
}

bool UTDSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 SlotIndex, int32 ActiveSlotIndex, FDropItem& DropItem)
{
	if(GetDropItemInfoFromInventory(SlotIndex, DropItem))
	{
		WeaponSlots[SlotIndex] = NewWeapon;
		if(SlotIndex == ActiveSlotIndex)
		{
			bool Result = SwitchWeaponByIndex(SlotIndex, -1, NewWeapon.AdditionalInfo);
			if(Result)
			{
				OnUpdateWeaponSlots.Broadcast(SlotIndex, WeaponSlots[SlotIndex]);
			}
			return Result;
		}
	}
	return false;
}

bool UTDSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeaponSlot)
{
	int32 FreeSlot = -1;
	if(CheckCanTakeWeapon(FreeSlot))
	{
		if(WeaponSlots.IsValidIndex(FreeSlot))
		{
			WeaponSlots[FreeSlot] = NewWeaponSlot;
			
			OnUpdateWeaponSlots.Broadcast(FreeSlot, NewWeaponSlot);
			return true;
		}
	}
	return false;
}

bool UTDSInventoryComponent::GetDropItemInfoFromInventory(int32 SlotIndex, FDropItem& DropItem)
{
	if(!WeaponSlots.IsValidIndex(SlotIndex))
		return false;

	FName DropItemName = GetWeaponNameBySlotIndex(SlotIndex);
	
	auto MyGI =Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if(MyGI)
	{
		if(MyGI->TryGetDropItemInfoByWeaponName(DropItem, DropItemName))
		{
			DropItem.WeaponInfo.AdditionalInfo = WeaponSlots[SlotIndex].AdditionalInfo;
			return true;
		};
	}
	return false;
}

void UTDSInventoryComponent::DropWeaponByIndex(int32 ByIndex, FDropItem& DropItemInfo)
{
	FWeaponSlot EmptyWeaponSlot;

	bool IsCanDrop = false;
	int8 i=0;
	int8 AviableWeaponNum = 0;
	while (i < WeaponSlots.Num() && !IsCanDrop)
	{
		if(!WeaponSlots[i].NameItem.IsNone())
		{
			AviableWeaponNum++;
			if(AviableWeaponNum > 1)
				IsCanDrop = true;
		}
	}

	if(IsCanDrop
		&& WeaponSlots.IsValidIndex(ByIndex)
		&& GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		//GetDropItemInfoFromInventory(ByIndex, DropItemInfo); //Already executed in if

		//Switch weapon to valid slot weapon from start array
		bool IsFindWeapon = false;
		int8 j=0;
		while (j < WeaponSlots.Num() && !IsFindWeapon)
		{
			if(!WeaponSlots[j].NameItem.IsNone())
			{
				OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
			}
			j++;
		}

		WeaponSlots[ByIndex] = EmptyWeaponSlot;
		if(GetOwner()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
		{
			ITDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}

		OnUpdateWeaponSlots.Broadcast(ByIndex, EmptyWeaponSlot);
	}
}

void UTDSInventoryComponent::InitInventory(TArray<FWeaponSlot> NewWeaponSlots, TArray<FAmmoSlot> NewAmmoSlots)
{
	WeaponSlots = NewWeaponSlots;
	AmmoSlots = NewAmmoSlots;
	
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if(myGI)
	{
		for(int8 i=0; i<WeaponSlots.Num(); i++)
		{
			auto& Slot = WeaponSlots[i];
			if(!Slot.NameItem.IsNone())
			{
				/*
				FWeaponInfo Info;
				if(myGI->TryGetWeaponInfoByName(Info, Slot.NameItem))
				{
					Slot.AdditionalInfo.Round = Info.MaxRound;
				}
				*/
			}
		}
	}

	MaxSlotWeapon = WeaponSlots.Num();

	if(WeaponSlots.IsValidIndex(0))
	{
		auto& FirstSlot = WeaponSlots[0];
		if(!FirstSlot.NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(FirstSlot.NameItem, FirstSlot.AdditionalInfo, 0);
		}
	}
}

TArray<FWeaponSlot> UTDSInventoryComponent::GetWeaponSlots() const
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UTDSInventoryComponent::GetAmmoSlots() const
{
	return AmmoSlots;
}


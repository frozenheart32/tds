﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSHealthComponent.h"
#include "TDSCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSCharacterHealthComponent : public UTDSHealthComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTDSCharacterHealthComponent();
	
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category="Shield")
	FOnShieldChange OnShieldChange;
	
protected:
	
	FTimerHandle TimerHandle_CooldownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryTimer;
	
	float Shield = 100.f;
	UPROPERTY(EditDefaultsOnly, Category="Shield")
	float ShieldRecovery = 1.f;
	UPROPERTY(EditDefaultsOnly, Category="Shield")
	float ShieldRecoveryRate = 0.5f;
	UPROPERTY(EditDefaultsOnly, Category="Shield")
	float CooldownShieldRecoveryTime = 5.f;
	
public:
	virtual void ChangeHealthValue(float ChangeValue) override;
	
	UFUNCTION(BlueprintCallable)
	float GetCurrentShield() const;

	UFUNCTION()
	void CooldownShieldEnd();
	void ChangeShieldValue(float ChangeValue);
	UFUNCTION()
	void RecoveryShield();
};

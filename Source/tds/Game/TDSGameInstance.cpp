// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/TDSGameInstance.h"

UTDSGameInstance::UTDSGameInstance()
{
	
}

void UTDSGameInstance::Init()
{
	Super::Init();
	ModelRepository = NewObject<UModelRepository>(this, UModelRepository::StaticClass(),
		FName{"ModelRepository"});
	WindowService = NewObject<UWindowService>(this, UWindowService::StaticClass(), FName{"WindowService"});
	WindowService->Initialize(ModelRepository);
}

bool UTDSGameInstance::TryGetWeaponInfoByName(FWeaponInfo& OutInfo, const FName& WeaponName)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow = nullptr;
	
	if(!weaponDataTable)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::TryGetWeaponInfoByName. weaponDataTable == NULL"));
		return false;
	}
	
	WeaponInfoRow = weaponDataTable->FindRow<FWeaponInfo>(WeaponName, "", false);
	if(WeaponInfoRow)
	{
		bIsFind = true;
		OutInfo = *WeaponInfoRow;
	}
	
	return bIsFind;
}

bool UTDSGameInstance::TryGetDropItemInfoByWeaponName(FDropItem& OutInfo, const FName& ItemName)
{
	FDropItem* DropItemRow = nullptr;
	
	if(!DropItemDataTable)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::TryGetDropItemInfoByName. DropItemDataTable == NULL"));
		return false;
	}
	
	TArray<FName> Names = DropItemDataTable->GetRowNames();
	for (int32 i = 0; i < Names.Num(); i++)
	{
		DropItemRow = DropItemDataTable->FindRow<FDropItem>(Names[i], "");
		if(DropItemRow->WeaponInfo.NameItem == ItemName)
		{
			OutInfo = *DropItemRow;
			return true;
		}
	}
	
	return false;
}

bool UTDSGameInstance::TryGetDropItemInfoByName(FDropItem& OutInfo, const FName& ItemName)
{
	if(!DropItemDataTable)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::TryGetWeaponInfoByName. weaponDataTable == NULL"));
		return false;
	}
	
	bool bIsFind = false;
	FDropItem* DropItemRow = nullptr;
	
	DropItemRow = DropItemDataTable->FindRow<FDropItem>(ItemName, "", false);
	if(DropItemRow)
	{
		bIsFind = true;
		OutInfo = *DropItemRow;
	}
	
	return bIsFind;
}

bool UTDSGameInstance::TryGetColorByWeaponName(FWeaponColor& OutInfo, const FName& ItemName)
{
	if(!WeaponColorDataTable)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::TryGetWeaponInfoByName. weaponDataTable == NULL"));
		return false;
	}
	
	bool bIsFind = false;
	FWeaponColor* WeaponColor = nullptr;
	
	WeaponColor = WeaponColorDataTable->FindRow<FWeaponColor>(ItemName, "", false);
	if(WeaponColor)
	{
		bIsFind = true;
		OutInfo = *WeaponColor;
	}
	
	return bIsFind;
}

bool UTDSGameInstance::TryGetColorByWeaponType(FWeaponColor& OutInfo, const EWeaponType& WeaponType)
{
	FWeaponColor* WeaponColor = nullptr;
	
	if(!WeaponColorDataTable)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::TryGetDropItemInfoByName. DropItemDataTable == NULL"));
		return false;
	}
	
	TArray<FName> Names = WeaponColorDataTable->GetRowNames();
	for (int32 i = 0; i < Names.Num(); i++)
	{
		WeaponColor = WeaponColorDataTable->FindRow<FWeaponColor>(Names[i], "");
		if(WeaponColor->WeaponType == WeaponType)
		{
			OutInfo = *WeaponColor;
			return true;
		}
	}
	
	return false;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "../FuncLibrary/Types.h"
#include "tds/MyServices/Window/WindowService.h"
#include "tds/UI/MVVMLibrary/ModelRepository.h"
#include "TDSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UTDSGameInstance();

protected:
	UPROPERTY(EditDefaultsOnly, Category="WeaponSettings")
	UDataTable* weaponDataTable = nullptr;
	
	UPROPERTY(EditDefaultsOnly, Category="Drop Item")
	UDataTable* DropItemDataTable = nullptr;

	UPROPERTY(EditDefaultsOnly, Category="Drop Item")
	UDataTable* WeaponColorDataTable = nullptr;
	
public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Services")
	UModelRepository* ModelRepository = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Services")
	UWindowService* WindowService = nullptr;

	virtual void Init() override;

	UFUNCTION(BlueprintCallable)
	bool TryGetWeaponInfoByName(FWeaponInfo& OutInfo, const FName& WeaponName);
	UFUNCTION(BlueprintCallable)
	bool TryGetDropItemInfoByWeaponName(FDropItem& OutInfo, const FName& ItemName);
	UFUNCTION(BlueprintCallable)
	bool TryGetDropItemInfoByName(FDropItem& OutInfo, const FName& ItemName);
	UFUNCTION(BlueprintCallable)
	bool TryGetColorByWeaponName(FWeaponColor& OutInfo, const FName& ItemName);
	UFUNCTION(BlueprintCallable)
	bool TryGetColorByWeaponType(FWeaponColor& OutInfo, const EWeaponType& WeaponType);
	
};

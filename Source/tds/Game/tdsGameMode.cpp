// Copyright Epic Games, Inc. All Rights Reserved.

#include "tdsGameMode.h"
#include "tdsPlayerController.h"
#include "../Character/tdsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AtdsGameMode::AtdsGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AtdsPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	DefaultPawnClass = AtdsCharacter::StaticClass();
}

void AtdsGameMode::PlayerCharacterDead()
{
	
}

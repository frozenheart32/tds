// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "tds/Buff/TDS_StateEffect.h"
#include "Types.generated.h"


UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName="Aim State"),
	AimWalk_State UMETA(DisplayName="AimWalk State"),
	Walk_State UMETA(DisplayName="Walk state"),
	Run_State UMETA(DisplayName="Run state"),
	Sprint_State UMETA(DisplayName="Sprint Run state"),
	Fatigue_State UMETA(DisplayName="Fatigue state"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RiffleType UMETA(DisplayName="Riffle"),
	ShotGunType UMETA(DisplayName="ShotGun"),
	Grenade_Launcher UMETA(DisplayName="GrenadeLauncher"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float AimSpeedNormal = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float WalkSpeedNormal = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float RunSpeedNormal = 600.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float AimWalkSpeed = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float SprintRunSpeed = 800.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float SprintRunDuration= 3.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float SprintAngleOffsetLimit= 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float FatigueSpeed = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float FatigueDuration= 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float Acceleration = 400.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float Breaking = 200.f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileMesh")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileMesh")
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileMesh")
	FTransform ProjectileStaticMeshOffset = FTransform{};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileMesh")
	UParticleSystem* ProjectileTrailFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileMesh")
	FTransform ProjectileTrailFXOffset = FTransform{};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileSettings")
	float ProjectileDamage = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileSettings")
	float ProjectileLifeTime = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "ProjectileSettings")
	float ProjectileInitSpeed = 2000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecalDictionary;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "FX")
	USoundBase* HitSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXDictionary;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Effect")
	TSubclassOf<UTDS_StateEffect> Effect;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Explode")
	UParticleSystem* ExplodeFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Explode")
	USoundBase* ExplodeSound = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Explode")
	float ProjectileMinRadiusDamage = 40.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Explode")
	float ProjectileMaxRadiusDamage = 2000.f;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Explode")
	float ExplodeMaxDamage = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Explode")
	float ExplodeFalloffCoef = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Explode")
	float TimeToExplode = 3.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Explode")
	bool isDebugExplode = false;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Aim_StateDispersionMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Aim_StateDispersionMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Aim_StateDispersionRecoil = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Aim_StateDispersionReduction = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float AimWalk_StateDispersionMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float AimWalk_StateDispersionMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float AimWalk_StateDispersionRecoil = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Walk_StateDispersionMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Walk_StateDispersionMin = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Walk_StateDispersionRecoil = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Run_StateDispersionMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Run_StateDispersionMin = 4.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Run_StateDispersionRecoil = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	float Run_StateDispersionReduction = 0.1f;
	
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	float DropMeshTime = -1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	float DropMeshLifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	FTransform DropMeshOffset = FTransform{};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	FVector DropMeshImpulse = FVector{0.f};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	float ImpulsePower = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	float ImpulseRandomDispersion = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Drop Mesh ")
	float CustomMass = 0.f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Class")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "State")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "State")
	float ReloadTime = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "State")
	int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "State")
	int32 NumberProjectileByShoot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Dispersion")
	FWeaponDispersion DispersionWeapon;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Sound")
	USoundBase* SoundReloadWeapon = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "FX")
	UParticleSystem* EffectFireWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Projectile")
	FProjectileInfo ProjectileSettings;
	
	//if projectile == nullptr then use trace settings
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Trace")
	float WeaponDamage = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Trace")
	float TraceDistance = 2000.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Hit Effect")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Owner animation")
	UAnimMontage* AnimOwnerFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Owner animation")
	UAnimMontage* AnimOwnerReload = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Weapon animation")
	UAnimMontage* AnimWeaponFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Weapon animation")
	UAnimMontage* AnimWeaponReload = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Mesh")
	FDropMeshInfo ClipDropMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Mesh")
	FDropMeshInfo ShellBullets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Inventory")
	float SwitchTimeToWeapon = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Inventory")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Inventory")
	EWeaponType WeaponType = EWeaponType::RiffleType;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "WeaponSettings")
	int32 Round = 32;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSlot")
	FName NameItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo;
	
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="WeaponSlot")
	EWeaponType WeaponType = EWeaponType::RiffleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AmmoSlot")
	int32 Count = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AmmoSlot")
	int32 MaxCount = 100;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* StaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMesh* SkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* ParticleItem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Offset")
	FTransform Offset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FWeaponSlot WeaponInfo;
};

USTRUCT(BlueprintType)
struct FWeaponColor: public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType WeaponType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor Color;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor, FName HitBoneName, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface Surface);
};

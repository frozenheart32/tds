// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"

#include "../Interface/TDS_IGameActor.h"


void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName HitBoneName, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface Surface)
{
	if(Surface != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTDS_StateEffect* MyEffect = Cast<UTDS_StateEffect>(AddEffectClass->GetDefaultObject());
		if(MyEffect)
		{
			bool IsHavePossibleSurface = false;
			int8 i = 0;
			while (i < MyEffect->PossibleInteractSurfaces.Num() && !IsHavePossibleSurface)
			{
				if(MyEffect->PossibleInteractSurfaces[i] == Surface)
				{
					IsHavePossibleSurface = true;
					bool IsCanAddEffect = false;
					if(!MyEffect->IsStackable)
					{
						int8 j = 0;
						TArray<UTDS_StateEffect*> CurrentEffects;
						ITDS_IGameActor* MyInterface = Cast<ITDS_IGameActor>(TakeEffectActor);
						if(MyInterface)
						{
							CurrentEffects = MyInterface->GetAllCurrentEffects();
						}

						if(CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !IsCanAddEffect)
							{
								if(CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									IsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							IsCanAddEffect = true;
						}
					}

					if(IsCanAddEffect)
					{
						UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(TakeEffectActor, AddEffectClass);
						if(NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, HitBoneName);
						}
					}
				}
				i++;
			}
		}
	}
}

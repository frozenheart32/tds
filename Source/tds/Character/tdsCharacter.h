// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLibrary/Types.h"
#include "../ActorComponents/TDSInventoryComponent.h"
#include "../Items/GameActor/Weapons/WeaponDefault.h"
#include "../ActorComponents/TDSCharacterHealthComponent.h"
#include "../Interface/TDS_IGameActor.h"
#include "tdsCharacter.generated.h"

UCLASS(Blueprintable)
class AtdsCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

public:
	AtdsCharacter();

	virtual void BeginPlay() override;
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void SetupPlayerInputComponent(UInputComponent* NewPlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:

	//Effects
	TArray<UTDS_StateEffect*> Effects;

	FTimerHandle RagDollTimerHandle;
	
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Health")
	UTDSCharacterHealthComponent* HealthComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Inventory")
	UTDSInventoryComponent* InventoryComponent = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Cursor")
	FVector CursorSize = FVector(20.f, 40.f, 40.f);

	UDecalComponent* CurrentCursor = nullptr;
	APlayerController* PlayerController = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	FCharacterSpeed MovementInfo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool AimEnabled = false;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Death")
	bool isAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Death")
	TArray<UAnimMontage*> DeathMontages;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Demo")
	FName InitWeaponName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Ability")
	TSubclassOf<UTDS_StateEffect> AbilityEffect;
	
	//Weapon
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="PickUp")
	int32 CurrentWeaponIndex;
	
	UFUNCTION()
	void InputAxisX(float X);
	UFUNCTION()
	void InputAxisY(float Y);

	//Movement tick
	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();
	UFUNCTION()
	void TryReloadWeapon();
	UFUNCTION()
	void TrySwitchNextWeapon();
	UFUNCTION()
	void TrySwitchPreviousWeapon();

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* CharacterReloadMontage);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool IsSuccess);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* CharacterFireMontage);

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

private:
	bool fatigueEnabled;
	float currentFatigueCooldown;
	float currentSprintDuration;
	FVector lastCursorPosition;
	
	void RotateToCursor();
	void CheckMovementStates();
	void CheckSprintDuration(const float& DeltaTime);
	void CheckSprintCooldown(const float& DeltaTime);
	void CheckSprintDirection();

	UFUNCTION()
	void InitWeapon(FName IdWeapon, FAdditionalWeaponInfo AdditionalWeaponInfo, int32 NewCurrentWeaponIndex);

	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon() const;
	
	void UpdateWeaponState();
	void CorrectWeaponShootEndLocation(const FHitResult& HitResult);

	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* CharacterReloadMontage);
	UFUNCTION()
	void WeaponReloadEnd(bool IsSuccess, int32 AmmoTake);
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* CharacterFireMontage);
	
	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);
	
	UFUNCTION()
	void DropCurrentWeapon();

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION()
	void CharDead();
	
	UFUNCTION()
	void EnableRagDoll();

	//Interface
	virtual EPhysicalSurface GetSurfaceType() override;
	virtual TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
	virtual void AddEffect(UTDS_StateEffect* NewEffect) override;
	virtual void RemoveEffect(UTDS_StateEffect* RemovedEffect) override;
	//End interface

	UFUNCTION()
	void TryAbilityEnabled();
	
	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}
};


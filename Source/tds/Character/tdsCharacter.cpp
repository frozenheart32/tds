// Copyright Epic Games, Inc. All Rights Reserved.

#include "tdsCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "tds/Game/TDSGameInstance.h"
#include "tds/Items/GameActor/Weapons/Projectiles/ProjectileDefault.h"

AtdsCharacter::AtdsCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
	HealthComponent = CreateDefaultSubobject<UTDSCharacterHealthComponent>(TEXT("CharacterHealthComponent"));

	if(HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AtdsCharacter::CharDead);
	}
	
	if(InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AtdsCharacter::InitWeapon);
	}
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Set primary sprint duration
	currentSprintDuration = MovementInfo.SprintRunDuration;
}

void AtdsCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<APlayerController>(GetController());
	
	if(CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector{0});
	}
}

void AtdsCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if(CurrentCursor)
	{
		if(PlayerController)
		{
			FHitResult traceHitResult;
			PlayerController->GetHitResultUnderCursor(ECC_Visibility, true, traceHitResult);
			FVector cursorFV = traceHitResult.ImpactNormal;
			FRotator cursorR = cursorFV.Rotation();

			CurrentCursor->SetWorldLocation(traceHitResult.Location);
			CurrentCursor->SetWorldRotation(cursorR);
		}
	}
	
	MovementTick(DeltaSeconds);
}

void AtdsCharacter::SetupPlayerInputComponent(UInputComponent* NewPlayerInputComponent)
{
	Super::SetupPlayerInputComponent(NewPlayerInputComponent);

	InputComponent->BindAxis(TEXT("MoveForward"), this, &AtdsCharacter::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &AtdsCharacter::InputAxisY);
	InputComponent->BindAction(TEXT("FireEvent"),  EInputEvent::IE_Pressed, this, &AtdsCharacter::InputAttackPressed);
	InputComponent->BindAction(TEXT("FireEvent"),  EInputEvent::IE_Released, this, &AtdsCharacter::InputAttackReleased);
	InputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TryReloadWeapon);

	InputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TrySwitchNextWeapon);
	InputComponent->BindAction(TEXT("SwitchPreviousWeapon"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TrySwitchPreviousWeapon);

	InputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &AtdsCharacter::DropCurrentWeapon);

	
	InputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TryAbilityEnabled);

/*
	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);
	
	InputComponent->BindKey(HotKeys[1], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<1>);
	InputComponent->BindKey(HotKeys[2], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<2>);
	InputComponent->BindKey(HotKeys[3], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<3>);
	InputComponent->BindKey(HotKeys[4], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<4>);
	InputComponent->BindKey(HotKeys[5], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<5>);
	InputComponent->BindKey(HotKeys[6], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<6>);
	InputComponent->BindKey(HotKeys[7], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<7>);
	InputComponent->BindKey(HotKeys[8], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<8>);
	InputComponent->BindKey(HotKeys[9], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<9>);
	InputComponent->BindKey(HotKeys[0], EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<0>);
	*/
	
	InputComponent->BindAction(TEXT("ActionNum1"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<1>);
	InputComponent->BindAction(TEXT("ActionNum2"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<2>);
	InputComponent->BindAction(TEXT("ActionNum3"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<3>);
	InputComponent->BindAction(TEXT("ActionNum4"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<4>);
	InputComponent->BindAction(TEXT("ActionNum5"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<5>);
	InputComponent->BindAction(TEXT("ActionNum6"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<6>);
	InputComponent->BindAction(TEXT("ActionNum7"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<7>);
	InputComponent->BindAction(TEXT("ActionNum8"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<8>);
	InputComponent->BindAction(TEXT("ActionNum9"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<9>);
	InputComponent->BindAction(TEXT("ActionNum0"), EInputEvent::IE_Pressed, this, &AtdsCharacter::TKeyPressed<0>);
}

void AtdsCharacter::InputAxisX(float X)
{
	AxisX = X;
}

void AtdsCharacter::InputAxisY(float Y)
{
	AxisY = Y;
}

void AtdsCharacter::MovementTick(float DeltaTime)
{
	if(!isAlive) return;
	
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);
	if(MovementState == EMovementState::Sprint_State)
	{
		FVector myRotationVector = FVector(AxisX, AxisY, 0.f);
		FRotator myRotator = myRotationVector.ToOrientationRotator();
		SetActorRotation(FQuat(myRotator));
	}
	else
	{
		RotateToCursor();
	}

	CheckSprintDuration(DeltaTime);
	CheckSprintCooldown(DeltaTime);
}

void AtdsCharacter::CharacterUpdate()
{
	float resSpeed = 600.f;
	switch (MovementState)
	{
		case EMovementState::Aim_State:
			resSpeed = MovementInfo.AimSpeedNormal;
			break;
		case EMovementState::AimWalk_State:
			resSpeed = MovementInfo.AimWalkSpeed;
			break;
		case EMovementState::Walk_State:
			resSpeed = MovementInfo.WalkSpeedNormal;
			break;
		case EMovementState::Run_State:
			resSpeed = MovementInfo.RunSpeedNormal;
			break;
		case EMovementState::Sprint_State:
			resSpeed = MovementInfo.SprintRunSpeed;
			break;
		case EMovementState::Fatigue_State:
			resSpeed = MovementInfo.FatigueSpeed;
		default:
			break;
	}
	
	UCharacterMovementComponent* characterMovement = GetCharacterMovement();
	
	auto previousSpeed = characterMovement->MaxWalkSpeed;
	characterMovement->MaxAcceleration = previousSpeed < resSpeed ? MovementInfo.Acceleration : MovementInfo.Breaking;
	characterMovement->MaxWalkSpeed = resSpeed;
}

void AtdsCharacter::ChangeMovementState()
{
	CheckMovementStates();
	CharacterUpdate();
	UpdateWeaponState();
}

UDecalComponent* AtdsCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void AtdsCharacter::InputAttackPressed()
{
	if(isAlive)
		AttackCharEvent(true);
}

void AtdsCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AtdsCharacter::TryReloadWeapon()
{
	if(isAlive && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		auto CurrentRound = CurrentWeapon->GetWeaponRound();
		if(CurrentRound < CurrentWeapon->WeaponSettings.MaxRound && CurrentWeapon->CheckCanWeaponReload())
		{
			CurrentWeapon->InitReload();
		}
	}
}

void AtdsCharacter::TrySwitchNextWeapon()
{
	if(!InventoryComponent) return;
	
	if(CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		auto OldIndex = CurrentWeaponIndex;
		FAdditionalWeaponInfo OldInfo;

		if(CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;

			if(CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if(InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(OldIndex + 1, OldIndex, OldInfo, true))
		{
			
		}
	}
}

void AtdsCharacter::TrySwitchPreviousWeapon()
{
	if(!InventoryComponent) return;
	
	if(CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		auto OldIndex = CurrentWeaponIndex;
		FAdditionalWeaponInfo OldInfo;

		if(CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;

			if(CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if(InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(OldIndex - 1, OldIndex, OldInfo, false))
		{
			
		}
	}
}

void AtdsCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		//TODO:Check melee or range
		//GEngine->AddOnScreenDebugMessage(INDEX_NONE, 2.f, FColor::Green, FString::Printf(TEXT("Character->Call SetWeaponState. isFiring %d"), bIsFiring));
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp,Warning, TEXT("AtdsCharacter::AttackCharEvent. CurrentWeapon = NULL"));
	}
}

void AtdsCharacter::CharDead_BP_Implementation()
{
	//In-BP
}

void AtdsCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* CharacterFireMontage)
{
	//In-BP
}

void AtdsCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* CharacterReloadMontage)
{
	//In-BP
}


void AtdsCharacter::WeaponReloadEnd_BP_Implementation(bool IsSuccess)
{
	//In-BP
}

void AtdsCharacter::RotateToCursor()
{
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if(myController)
	{
		FHitResult HitResult;
		//Trace Query Type - костыль. Запомнить, что такое есть в UE.
		//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, HitResult);
		myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);
		FVector actorLocation = GetActorLocation();
		float findRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(actorLocation, HitResult.Location).Yaw;
		SetActorRotation(FRotator(0.0f,findRotatorResultYaw, 0.0f), ETeleportType::None);

		CorrectWeaponShootEndLocation(HitResult);
	}
}

void AtdsCharacter::CheckMovementStates()
{
	if(fatigueEnabled)
	{
		MovementState = EMovementState::Fatigue_State;
		return;
	}
	
	if(!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if(SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::Sprint_State;
		}
		else if(WalkEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if(WalkEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if(!WalkEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
}

void AtdsCharacter::CheckSprintDuration(const float& DeltaTime)
{
	if(SprintRunEnabled)
	{
		currentSprintDuration -= DeltaTime;
		if(currentSprintDuration <= 0.f)
		{
			currentSprintDuration = 0;
			currentFatigueCooldown = MovementInfo.FatigueDuration;
			fatigueEnabled = true;
			ChangeMovementState();
		}
		else
		{
			CheckSprintDirection();
		}
	}
}

void AtdsCharacter::CheckSprintCooldown(const float& DeltaTime)
{
	if(fatigueEnabled)
	{
		currentFatigueCooldown -= DeltaTime;
		if(currentFatigueCooldown <=0.f)
		{
			currentFatigueCooldown = 0.f;
			currentSprintDuration = MovementInfo.SprintRunDuration;
			fatigueEnabled = false;
			ChangeMovementState();
		}
	}
}

void AtdsCharacter::CheckSprintDirection()
{
	auto characterMovement = GetCharacterMovement();
	FVector forwardDirection = UKismetMathLibrary::Normal(characterMovement->Velocity);
	FVector currentCharacterPosition = GetActorLocation();
	FVector targetPosition = FVector{lastCursorPosition.X, lastCursorPosition.Y, currentCharacterPosition.Z};
	FVector targetDirection = UKismetMathLibrary::Normal(targetPosition - currentCharacterPosition);
	auto dot = UKismetMathLibrary::Dot_VectorVector(forwardDirection, targetDirection);

	float angle = UKismetMathLibrary::Abs(UKismetMathLibrary::DegAcos(dot));
	
	if(angle > MovementInfo.SprintAngleOffsetLimit)
	{
		SprintRunEnabled = false;
		ChangeMovementState();
	}
	//GEngine->AddOnScreenDebugMessage(INDEX_NONE, 0.1f, FColor::Yellow, FString::Printf(TEXT("angle: %f, offset: %f"), angle, MovementInfo.SprintAngleOffsetLimit));
}

void AtdsCharacter::InitWeapon(FName IdWeapon, FAdditionalWeaponInfo AdditionalWeaponInfo, int32 NewCurrentWeaponIndex)
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	
	UTDSGameInstance* GameInstance = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo WeaponInfo;
	bool Result = false;
	
	if(GameInstance)
	{
		Result =  GameInstance->TryGetWeaponInfoByName(WeaponInfo, IdWeapon);
	}
	
	if(Result)
	{
		FVector SpawnLocation = FVector{0};
		FRotator SpawnRotator = FRotator{0};

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParameters.Owner = this;
		SpawnParameters.Instigator = GetInstigator();

		AWeaponDefault* MyWeapon = Cast<AWeaponDefault>(
			GetWorld()->
			SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotator, SpawnParameters));

		if(MyWeapon)
		{
			FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);
			MyWeapon->AttachToComponent(GetMesh(), Rules, FName("WeaponSocketRightHand"));
			MyWeapon->WeaponSettings = WeaponInfo;
			MyWeapon->AdditionalWeaponInfo = AdditionalWeaponInfo;
			MyWeapon->ReloadTime = MyWeapon->WeaponSettings.ReloadTime;

			CurrentWeapon = MyWeapon;
			MyWeapon->UpdateWeaponState(MovementState);

			
			CurrentWeaponIndex = NewCurrentWeaponIndex;
			
			
			MyWeapon->OnWeaponReloadStart.AddDynamic(this, &AtdsCharacter::WeaponReloadStart);
			MyWeapon->OnWeaponReloadEnd.AddDynamic(this, &AtdsCharacter::WeaponReloadEnd);
			MyWeapon->OnWeaponFireStart.AddDynamic(this, &AtdsCharacter::WeaponFireStart);

			if(CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
				CurrentWeapon->InitReload();

			if(InventoryComponent)
				InventoryComponent->OnWeaponAmmoAviable.Broadcast(CurrentWeapon->WeaponSettings.WeaponType);
		}
	}
	else
	{
		UE_LOG(LogTemp,Warning, TEXT("AtdsCharacter::InitWeapon. WeaponInfo not found in table"));
	}
}

AWeaponDefault* AtdsCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void AtdsCharacter::UpdateWeaponState()
{
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		myWeapon->UpdateWeaponState(MovementState);
	}
}

void AtdsCharacter::CorrectWeaponShootEndLocation(const FHitResult& HitResult)
{
	if(CurrentWeapon)
	{
		FVector Displacement{0.f};
		switch (MovementState)
		{
		case EMovementState::Aim_State:
			Displacement = FVector{0.f, 0.f, 160.f};
			CurrentWeapon->ShouldReduceDispersion = true;
			break;
		case EMovementState::AimWalk_State:
			Displacement = FVector{0.f, 0.f, 160.f};
			CurrentWeapon->ShouldReduceDispersion = true;
			break;
		case EMovementState::Walk_State:
			Displacement = FVector{0.f, 0.f, 120.f};
			CurrentWeapon->ShouldReduceDispersion = false;
			break;
		case EMovementState::Run_State:
			Displacement = FVector{0.f, 0.f, 120.f};
			CurrentWeapon->ShouldReduceDispersion = false;
			break;
		default:
			CurrentWeapon->ShouldReduceDispersion = false;
			break;
		}

		CurrentWeapon->ShootEndLocation = HitResult.Location + Displacement;
	}
}

void AtdsCharacter::WeaponReloadStart(UAnimMontage* CharacterReloadMontage)
{
	WeaponReloadStart_BP(CharacterReloadMontage);
}

void AtdsCharacter::WeaponReloadEnd(bool IsSuccess, int32 AmmoTake)
{
	if(IsSuccess && InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->
			AmmoSlotChangeValue(CurrentWeapon->WeaponSettings.WeaponType, AmmoTake);
		InventoryComponent->
			SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(IsSuccess);
}

void AtdsCharacter::WeaponFireStart(UAnimMontage* CharacterFireMontage)
{
	if(InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponFireStart_BP(CharacterFireMontage);
}

bool AtdsCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentWeaponIndex != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentWeaponIndex;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}	
	return bIsSuccess;
}

void AtdsCharacter::DropCurrentWeapon()
{
	if(InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex(CurrentWeaponIndex, ItemInfo);
	}
}

float AtdsCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                                AActor* DamageCauser)
{
	float Damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	
	if(isAlive)
		HealthComponent->ChangeHealthValue(-Damage);


	if(DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		auto ProjectileDefault = Cast<AProjectileDefault>(DamageCauser);
		if(ProjectileDefault)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None,
				ProjectileDefault->Projectilesettings.Effect, GetSurfaceType());
		}
	}
	return  Damage;
}

void AtdsCharacter::CharDead()
{
	float TimeAnim = 0.f;
	int32 random = FMath::RandHelper(DeathMontages.Num());
	auto MyMesh = GetMesh();
	if(DeathMontages.IsValidIndex(random) && MyMesh)
	{
		TimeAnim = DeathMontages[random]->GetPlayLength();
		MyMesh->GetAnimInstance()->Montage_Play(DeathMontages[random]);
	}

	auto MyController = GetController();
	if(MyController)
	{
		MyController->UnPossess();
	}
	
	UnPossessed();
	isAlive = false;
	
	//Timer for regdoll
	GetWorldTimerManager().SetTimer(RagDollTimerHandle, this, &AtdsCharacter::EnableRagDoll, TimeAnim);
	GetCursorToWorld()->SetVisibility(false);
	AttackCharEvent(false);
	
	CharDead_BP();
}

void AtdsCharacter::EnableRagDoll()
{
	auto MyMesh = GetMesh();
	if(MyMesh)
	{
		MyMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		MyMesh->SetSimulatePhysics(true);
	}
}

EPhysicalSurface AtdsCharacter::GetSurfaceType()
{
	auto Result = EPhysicalSurface::SurfaceType_Default;
	if(HealthComponent)
	{
		if(HealthComponent->GetCurrentShield() <= 0)
		{
			auto MyMesh = GetMesh();
			if(MyMesh)
			{
				auto MyMaterial = MyMesh->GetMaterial(0);
				if(MyMaterial)
				{
					Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UTDS_StateEffect*> AtdsCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void AtdsCharacter::AddEffect(UTDS_StateEffect* NewEffect)
{
	if(NewEffect)
	{
		Effects.Add(NewEffect);
	}
}

void AtdsCharacter::RemoveEffect(UTDS_StateEffect* RemovedEffect)
{
	if(RemovedEffect)
	{
		if(Effects.Contains(RemovedEffect))
		{
			Effects.Remove(RemovedEffect);
		}
	}
}

void AtdsCharacter::TryAbilityEnabled()
{
	//TODO: Add Cooldown
	if(AbilityEffect)
	{
		UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(this, AbilityEffect);
		if(NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}



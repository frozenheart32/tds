﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "tds/Interface/TDS_IGameActor.h"
#include "TDS_EnvironmentStructrure.generated.h"

UCLASS()
class TDS_API ATDS_EnvironmentStructrure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_EnvironmentStructrure();

	//Effects
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="StateEffects")
	TArray<UTDS_StateEffect*> Effects;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual EPhysicalSurface GetSurfaceType() override;
	virtual TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
	virtual void AddEffect(UTDS_StateEffect* NewEffect) override;
	virtual void RemoveEffect(UTDS_StateEffect* RemovedEffect) override;
};

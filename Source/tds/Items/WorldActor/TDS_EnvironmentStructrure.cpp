﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnvironmentStructrure.h"

#include "PhysicalMaterials/PhysicalMaterial.h"


// Sets default values
ATDS_EnvironmentStructrure::ATDS_EnvironmentStructrure()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATDS_EnvironmentStructrure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_EnvironmentStructrure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

EPhysicalSurface ATDS_EnvironmentStructrure::GetSurfaceType()
{
	auto Result = EPhysicalSurface::SurfaceType_Default;
	auto MyMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if(MyMesh)
	{
		auto MyMaterial = MyMesh->GetMaterial(0);
		if(MyMaterial)
		{
			Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	
	return Result;
}

TArray<UTDS_StateEffect*> ATDS_EnvironmentStructrure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_EnvironmentStructrure::AddEffect(UTDS_StateEffect* NewEffect)
{
	if(NewEffect)
	{
		Effects.Add(NewEffect);
	}
}

void ATDS_EnvironmentStructrure::RemoveEffect(UTDS_StateEffect* RemovedEffect)
{
	if(RemovedEffect)
	{
		if(Effects.Contains(RemovedEffect))
		{
			Effects.Remove(RemovedEffect);
		}
	}
}



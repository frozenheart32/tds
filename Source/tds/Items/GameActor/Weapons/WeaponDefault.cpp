﻿#include "WeaponDefault.h"

#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Perception/AISense_Damage.h"
#include "Projectiles/ProjectileDefault.h"
#include "tds/ActorComponents/TDSInventoryComponent.h"


// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	WeaponInit();
	Super::BeginPlay();
}

void AWeaponDefault::StartWeaponFireAnim_BP_Implementation(UAnimMontage* Montage)
{
	//In-BP
}

void AWeaponDefault::StartWeaponReloadAnim_BP_Implementation(UAnimMontage* Montage)
{
	//In-BP
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
	float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if(DropMesh)
	{
		FTransform Transform;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X
		+ this->GetActorRightVector() * Offset.GetLocation().Y
		+ this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		AStaticMeshActor* NewActor = nullptr;

		FActorSpawnParameters Params;
		Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Params.Owner = this;

		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Params);
		
		if(NewActor && NewActor->GetStaticMeshComponent())
		{
			auto NewStaticMeshComponent = NewActor->GetStaticMeshComponent();
			
			NewStaticMeshComponent->SetCollisionProfileName(TEXT("IgnoredPawn"));
			NewStaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			//NewActor->SetActorTickEnabled(true);
			//Set parameter for new actor
			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = LifeTimeMesh;

			NewStaticMeshComponent->Mobility = EComponentMobility::Movable;
			NewStaticMeshComponent->SetSimulatePhysics(true);
			NewStaticMeshComponent->SetStaticMesh(DropMesh);

			//NewStaticMeshComponent->SetCollisionObjectType();

			NewStaticMeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewStaticMeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewStaticMeshComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewStaticMeshComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewStaticMeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewStaticMeshComponent->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);


			if(CustomMass > 0.f)
			{
				NewStaticMeshComponent->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if(!DropImpulseDirection.IsNearlyZero())
			{
				auto RightVector = NewStaticMeshComponent->GetForwardVector() * DropImpulseDirection.X * 1000.f
					+ NewStaticMeshComponent->GetRightVector() * DropImpulseDirection.Y * 1000.f
					+ this->GetActorUpVector() * DropImpulseDirection.Z * 1000.f;
				
				FVector FinalDir;

				if(!FMath::IsNearlyZero(ImpulseRandomDispersion))
				{
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(RightVector, ImpulseRandomDispersion);
				}

				FinalDir.GetSafeNormal(0.0001f);

				NewStaticMeshComponent->AddImpulse(FinalDir * PowerImpulse);
			}
		}
	}
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ShellDropTick(DeltaTime);
	DropClipTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if(WeaponFiring)
	{
		if(GetWeaponRound() > 0 && !WeaponReloading)
		{
			if(FireTimer < 0.f)
			{
				Fire();
			}
			else
			{
				FireTimer -= DeltaTime;
			}
		}
		/*
		else
		{
			if(!WeaponReloading && CheckCanWeaponReload())
			{
				InitReload();
			}
		}
		*/
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if(WeaponReloading)
	{
		if(ReloadTimer > 0.f)
		{
			ReloadTimer -= DeltaTime;
		}
		else
		{
			FinishReload();
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if(WeaponReloading) return;

	if(!WeaponFiring)
	{
		if(ShouldReduceDispersion)
		{
			CurrentDispersion -= CurrentDispersionReduction;
		}
		else
		{
			CurrentDispersion += CurrentDispersionReduction;
		}
	}

	if(CurrentDispersion < CurrentDispersionMin)
	{
		CurrentDispersion = CurrentDispersionMin;
	}
	else if(CurrentDispersion > CurrentDispersionMax)
	{
		CurrentDispersion = CurrentDispersionMax;
	}

	if(isShowDebug)
	{
		UE_LOG(LogTemp, Warning, TEXT("Despersion: MAX = %f, MIN = %f, Current = %f"),
			CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if(DropShellFlag)
	{
		if(DropShellTimer < 0.f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSettings.ShellBullets.DropMesh,
				WeaponSettings.ShellBullets.DropMeshOffset, WeaponSettings.ShellBullets.DropMeshImpulse,
				WeaponSettings.ShellBullets.DropMeshLifeTime, WeaponSettings.ShellBullets.ImpulseRandomDispersion,
				WeaponSettings.ShellBullets.ImpulsePower, WeaponSettings.ShellBullets.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;	
		}
	}
}

void AWeaponDefault::DropClipTick(float DeltaTime)
{
	if(DropClipFlag)
	{
		if(DropClipTimer < 0.f)
		{
			DropClipFlag = false;
			InitDropMesh(WeaponSettings.ClipDropMesh.DropMesh,
				WeaponSettings.ClipDropMesh.DropMeshOffset, WeaponSettings.ClipDropMesh.DropMeshImpulse,
				WeaponSettings.ClipDropMesh.DropMeshLifeTime, WeaponSettings.ClipDropMesh.ImpulseRandomDispersion,
				WeaponSettings.ClipDropMesh.ImpulsePower, WeaponSettings.ClipDropMesh.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;	
		}
	}
}

void AWeaponDefault::WeaponInit()
{
	if(SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if(StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent(true);
	}
	
	if(IsValid(SkeletalMeshWeapon))
	{
		UsesSkeletalMesh = true;
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if(CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
		FireTimer = 0.01f;
	}
}

void AWeaponDefault::UpdateWeaponState(EMovementState State)
{
	BlockFire = false;
	switch (State)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Aim_StateDispersionMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Aim_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Aim_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Walk_StateDispersionMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Walk_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Walk_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Run_StateDispersionMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Run_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Run_StateDispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Run_StateDispersionReduction;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;;
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile() const
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSettings.RateOfFire;
	
	AdditionalWeaponInfo.Round--;
	ChangeDispersionByShoot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.EffectFireWeapon, ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();
	
	if(ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation;
		FProjectileInfo ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i<NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();
		
			if(ProjectileInfo.Projectile)
			{
				FireWithProjectile(ProjectileInfo, SpawnLocation, SpawnRotation, EndLocation);
			}
			else
			{
				FireWithTrace(ProjectileInfo, SpawnLocation, EndLocation);
			}

			if(WeaponSettings.AnimWeaponFire)
			{
				if(UsesSkeletalMesh)
				{
					StartWeaponFireAnim_BP(WeaponSettings.AnimWeaponFire);
				}
			}

			if(WeaponSettings.ShellBullets.DropMesh)
			{
				if(WeaponSettings.ShellBullets.DropMeshTime < 0.0f)
				{
					InitDropMesh(WeaponSettings.ShellBullets.DropMesh,
					WeaponSettings.ShellBullets.DropMeshOffset, WeaponSettings.ShellBullets.DropMeshImpulse,
					WeaponSettings.ShellBullets.DropMeshLifeTime, WeaponSettings.ShellBullets.ImpulseRandomDispersion,
					WeaponSettings.ShellBullets.ImpulsePower, WeaponSettings.ShellBullets.CustomMass);
				}
				else
				{
					DropShellFlag = true;
					DropShellTimer = WeaponSettings.ShellBullets.DropMeshTime;
				}
			}
					
			if(WeaponSettings.AnimOwnerFire)
				OnWeaponFireStart.Broadcast(WeaponSettings.AnimOwnerFire);
		}
	}

	if(GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if(CheckCanWeaponReload())
		{
			InitReload();
		}
	}
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	FVector EndLocation = FVector{0.f};
	FVector TmpV = ShootLocation->GetComponentLocation() - ShootEndLocation;
	float size = TmpV.Size();
	if(size > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() +
			ApplyDispersionToShoot((ShootLocation->GetComponentLocation() -
				ShootEndLocation).GetSafeNormal()) * -20000.f;
		
		if(isShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(),
				-(ShootLocation->GetComponentLocation()-ShootEndLocation),
				WeaponSettings.TraceDistance, GetCurrentDispersion() * PI /100.f,
				GetCurrentDispersion() * PI / 100.f, 32, FColor::Emerald,
				false, 0.1f, (uint8)'\000', 1.0f);
		}
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() +
			ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.f;

		if(isShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(),
				ShootLocation->GetForwardVector(),
				WeaponSettings.TraceDistance, GetCurrentDispersion() * PI /100.f,
				GetCurrentDispersion() * PI / 100.f, 32, FColor::Emerald,
				false, 0.1f, (uint8)'\000', 1.0f);
		}
	}

	if(isShowDebug)
	{
		FVector Location = ShootLocation->GetComponentLocation();
		//direction weapon look
		DrawDebugLine(GetWorld(), Location, Location + ShootLocation->GetForwardVector() * 500.f,
			FColor::Cyan);
		
		//direction project must fly
		DrawDebugLine(GetWorld(), Location, ShootEndLocation,
			FColor::Red, false, 5.f, (uint8)'\000', 0.5f);

		//direction projectile Current fly
		DrawDebugLine(GetWorld(), Location, EndLocation,
			FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), Location + ShootLocation->GetForwardVector() * SizeVectorToChangeShootDirectionLogic,
			//5.f, 32, FColor::Orange, false, 5.f, (uint8)'\000');
	}
	
	return EndLocation;
}

int32 AWeaponDefault::GetWeaponRound() const
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;
	
	if(WeaponSettings.AnimWeaponReload)
	{
		if(UsesSkeletalMesh)
		{
			StartWeaponReloadAnim_BP(WeaponSettings.AnimWeaponReload);
		}
	}

	if(WeaponSettings.AnimOwnerReload)
		OnWeaponReloadStart.Broadcast(WeaponSettings.AnimOwnerReload);

	if(WeaponSettings.ClipDropMesh.DropMesh)
	{
		DropClipFlag = true;
		DropClipTimer = WeaponSettings.ClipDropMesh.DropMeshTime;
	}
}

void AWeaponDefault::FinishReload()
{
	auto AviableAmmoFromInventory = GetAviableAmmoForReload();
	WeaponReloading = false;
	if(AviableAmmoFromInventory > WeaponSettings.MaxRound)
	{
		AviableAmmoFromInventory = WeaponSettings.MaxRound;
	}
	
	int32 AmmoNeedTake = AdditionalWeaponInfo.Round - AviableAmmoFromInventory;
	AdditionalWeaponInfo.Round -= AmmoNeedTake;
	OnWeaponReloadEnd.Broadcast(true, AmmoNeedTake);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if(SkeletalMeshWeapon)
	{
		auto AnimInstance = SkeletalMeshWeapon->GetAnimInstance();
		if(AnimInstance)
		{
			AnimInstance->StopAllMontages(0.15f);
		}
	}

	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool Result = true;
	auto MyOwner = GetOwner();
	if(MyOwner)
	{
		int32 AvaibleAmmo = 0;
		auto InventoryComponent =
			Cast<UTDSInventoryComponent>(MyOwner->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if(InventoryComponent)
		{
			if(!InventoryComponent->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvaibleAmmo))
			{
				InventoryComponent->
					OnWeaponNotHaveRound.Broadcast(InventoryComponent->GetWeaponSlotIndexByName(WeaponNameId));
				Result = false;
			}
			else
			{
				InventoryComponent->
					OnWeaponHaveRound.Broadcast(InventoryComponent->GetWeaponSlotIndexByName(WeaponNameId));
			}
		}
	}

	return Result;
}

int32 AWeaponDefault::GetAviableAmmoForReload()
{
	int32 AviableAmmo = WeaponSettings.MaxRound;
	auto MyOwner = GetOwner();
	if(MyOwner)
	{
		auto InventoryComponent =
			Cast<UTDSInventoryComponent>(MyOwner->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if(InventoryComponent)
		{
			InventoryComponent->CheckAmmoForWeapon(WeaponSettings.WeaponType, AviableAmmo);
		}
	}
	return AviableAmmo;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector ShootDirection) const
{
	return FMath::VRandCone(ShootDirection, GetCurrentDispersion() * PI / 100.f);
}

void AWeaponDefault::ChangeDispersionByShoot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShoot;
}

void AWeaponDefault::FireWithProjectile(const FProjectileInfo& ProjectileInfo, const FVector& SpawnLocation,
	FRotator SpawnRotation,const FVector& EndLocation)
{
	FVector Dir = EndLocation - SpawnLocation;
	Dir.Normalize();
		
	FMatrix myMatrix{Dir, FVector{0.f,1.f,0.f}, FVector{0.f,0.f,1.f}, FVector::ZeroVector};
	SpawnRotation = myMatrix.Rotator();
	
	//Projectile init ballistic fire
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = GetOwner();
	SpawnParameters.Instigator = GetInstigator();

	AProjectileDefault* myProjectile = Cast<AProjectileDefault>(
		GetWorld()->SpawnActor(ProjectileInfo.Projectile,
		&SpawnLocation, &SpawnRotation, SpawnParameters));

	if(myProjectile)
	{
		myProjectile->InitProjectile(ProjectileInfo);
	}
}

void AWeaponDefault::FireWithTrace(const FProjectileInfo& ProjectileInfo, const FVector& SpawnLocation, const FVector& EndLocation)
{
	FHitResult Hit;
	TArray<AActor*> Actors;

	UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSettings.TraceDistance,
		ETraceTypeQuery::TraceTypeQuery4, false, Actors, EDrawDebugTrace::ForDuration, Hit,
		true, FLinearColor::Red, FLinearColor::Green, 5.0f);

	if(isShowDebug)
	{
		DrawDebugLine(GetWorld(), SpawnLocation,
			SpawnLocation + ShootLocation->GetForwardVector() * WeaponSettings.TraceDistance,
			FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
	}

	if(Hit.GetActor() && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		if(WeaponSettings.ProjectileSettings.HitDecalDictionary.Contains(MySurfaceType))
		{
			UMaterialInterface* MyMaterial = WeaponSettings.ProjectileSettings.HitDecalDictionary[MySurfaceType];

			if(MyMaterial && Hit.GetComponent())
			{
				UGameplayStatics::SpawnDecalAttached(MyMaterial,
					FVector(20.f), Hit.GetComponent(), NAME_None,
					Hit.ImpactPoint, Hit.ImpactNormal.Rotation(),
					EAttachLocation::KeepWorldPosition);
			}

			if(WeaponSettings.ProjectileSettings.HitFXDictionary.Contains(MySurfaceType))
			{
				UParticleSystem* MyParticle = WeaponSettings.ProjectileSettings.HitFXDictionary[MySurfaceType];
				if(MyParticle)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
						MyParticle,
						FTransform{Hit.ImpactNormal.Rotation(),
							Hit.ImpactPoint,
							FVector{1.f}});
				}
			}

			if(WeaponSettings.ProjectileSettings.HitSound)
			{
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(),
					WeaponSettings.ProjectileSettings.HitSound, Hit.ImpactPoint);
			}
			
			UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileInfo.Effect, MySurfaceType);
			
/*
			//For All
			if(Hit.GetActor()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
			{
				ITDS_IGameActor::Execute_AviableForEffects(Hit.GetActor());

				ITDS_IGameActor::Execute_AviableForEffectsBP(Hit.GetActor());
			}
			else
			{
				
			}
			*/
			UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSettings.ProjectileSettings.ProjectileDamage,
				GetInstigatorController(), this, NULL);
			UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(),
			WeaponSettings.ProjectileSettings.ProjectileDamage,Hit.Location,
			Hit.Location);
		}
	}
}

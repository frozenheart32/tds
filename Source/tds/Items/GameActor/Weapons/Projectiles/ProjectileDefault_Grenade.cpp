// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	TimerExplodeTick(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplodeTick(float DeltaTime)
{
	if(TimerEnabled)
	{
		if(TimerToExplode > TimeToExplode)
		{
			Explode();
		}
		else
		{
			TimerToExplode += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& HitResult)
{
	if(!TimerEnabled)
	{
		Explode();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, HitResult);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explode()
{
	TimerEnabled = false;
	if(Projectilesettings.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(
			GetWorld(), Projectilesettings.ExplodeFX, GetActorLocation(),
			GetActorRotation(), FVector{1.f});
	}

	if(Projectilesettings.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(
			GetWorld(), Projectilesettings.ExplodeSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		Projectilesettings.ExplodeMaxDamage,
		Projectilesettings.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		Projectilesettings.ProjectileMinRadiusDamage,
		Projectilesettings.ProjectileMaxRadiusDamage,
		5, NULL,
		IgnoredActor, this, GetInstigatorController());
	

	if(Projectilesettings.isDebugExplode)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), Projectilesettings.ProjectileMinRadiusDamage, 32,
			FColor::Red, false, 4.f, (uint8)'\000', 1.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), Projectilesettings.ProjectileMaxRadiusDamage, 32,
			FColor::Orange, false, 4.f, (uint8)'\000', 1.f);
	}
	
	this->Destroy();
}

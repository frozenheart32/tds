// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"


UCLASS()
class TDS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	virtual void Tick(float DeltaTime) override;

	void TimerExplodeTick(float DeltaTime);

	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
								  FVector NormalImpulse, const FHitResult& HitResult) override;

	virtual void ImpactProjectile() override;
	void Explode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade settings")
	bool TimerEnabled = false;
	
	float TimerToExplode = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Grenade settings")
	float TimeToExplode = 5.f;
};

﻿#include "ProjectileDefault.h"

#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Perception/AISense_Damage.h"


// Sets default values
AProjectileDefault::AProjectileDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	BulletCollisionSphere->SetSphereRadius(10.f);

	BulletCollisionSphere->bReturnMaterialOnMove = true; //hit event return physic material

	BulletCollisionSphere->SetCanEverAffectNavigation(false); //collision not affect navigetion (P key on editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet projectile mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet Projectile Movement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	//Bind on events
	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollesionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileDefault::InitProjectile(FProjectileInfo Info)
{
	BulletProjectileMovement->InitialSpeed = Info.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = Info.ProjectileInitSpeed;
	auto Forward = BulletCollisionSphere->GetForwardVector();
	Forward.Normalize();
	BulletProjectileMovement->Velocity = Forward * Info.ProjectileInitSpeed;
	this->SetLifeSpan(Info.ProjectileLifeTime);

	if(Info.ProjectileStaticMesh)
	{
		BulletMesh->SetStaticMesh(Info.ProjectileStaticMesh);
		BulletMesh->SetRelativeTransform(Info.ProjectileStaticMeshOffset);
	}
	else
	{
		BulletMesh->DestroyComponent();
	}

	if(Info.ProjectileTrailFX)
	{
		BulletFX->SetTemplate(Info.ProjectileTrailFX);
	}
	else
	{
		BulletFX->DestroyComponent();
	}
	
	Projectilesettings = Info;
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
                                                  UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& HitResult)
{
	if(OtherActor && HitResult.PhysMaterial.IsValid())
	{
		EPhysicalSurface OtherSurfaceType = UGameplayStatics::GetSurfaceType(HitResult);

		if(Projectilesettings.HitDecalDictionary.Contains(OtherSurfaceType))
		{
			UMaterialInterface* DecalMaterial = Projectilesettings.HitDecalDictionary[OtherSurfaceType];

			if(DecalMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector{20.f}, OtherComp, NAME_None,
					HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(),
					EAttachLocation::KeepWorldPosition, 10.f);
			}

			if(Projectilesettings.HitFXDictionary.Contains(OtherSurfaceType))
			{
				UParticleSystem* myParticle = Projectilesettings.HitFXDictionary[OtherSurfaceType];
				if(myParticle)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle,
						FTransform{HitResult.ImpactNormal.Rotation(),
							HitResult.ImpactPoint,FVector{1}});
				}
			}

			if(Projectilesettings.HitSound)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), Projectilesettings.HitSound, HitResult.ImpactPoint);
			}
			
		}
		
		UTypes::AddEffectBySurfaceType(HitResult.GetActor(), HitResult.BoneName, Projectilesettings.Effect, OtherSurfaceType);
		
		//For test
		UGameplayStatics::ApplyPointDamage(OtherActor, Projectilesettings.ProjectileDamage, HitResult.TraceStart, HitResult,
			GetInstigatorController(), this, NULL);
		
		UAISense_Damage::ReportDamageEvent(GetWorld(), OtherActor, GetInstigator(),
			Projectilesettings.ProjectileDamage,HitResult.Location,
			HitResult.Location);
		
		ImpactProjectile();
	}
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	
}

void AProjectileDefault::BulletCollesionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}

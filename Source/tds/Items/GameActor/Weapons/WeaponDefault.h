﻿#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"
#include "tds/FuncLibrary/Types.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, OwnerMontage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, OwnerMontage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, IsSuccess, int32, AmmoNeedTake);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Category="Components")
	USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Category="Components")
	USkeletalMeshComponent* SkeletalMeshWeapon= nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Category="Components")
	UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Category="Components")
	UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon Info")
	FName WeaponNameId;
	
	UPROPERTY()
	FWeaponInfo WeaponSettings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Fire Logic")
	bool WeaponFiring = false;

	bool BlockFire = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Reload Logic")
	bool WeaponReloading = false;

	float FireTimer = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Reload Logic")
	float ReloadTimer = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Mesh info")
	bool UsesSkeletalMesh = false;

	//TODO:Remove it (Debug)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Reload Logic")
	float ReloadTime = 0.f;
	
	//TODO: Remove debug category
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
	bool isShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.f;
	
	FVector ShootEndLocation = FVector{0};

	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.f;
	float CurrentDispersionMin = 0.f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	bool DropShellFlag = false;
	float DropShellTimer = -1.f;

	bool DropClipFlag = false;
	float DropClipTimer = -1.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintNativeEvent)
	void StartWeaponFireAnim_BP(UAnimMontage* Montage);
	UFUNCTION(BlueprintNativeEvent)
	void StartWeaponReloadAnim_BP(UAnimMontage* Montage);

	UFUNCTION()
	void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
		float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);
	void DropClipTick(float DeltaTime);
	void WeaponInit();

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire);


	void UpdateWeaponState(EMovementState State);

	bool CheckWeaponCanFire();
	FProjectileInfo GetProjectile() const;
	
	void Fire();
	
	FVector GetFireEndLocation() const;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound() const;

	void InitReload();
	void FinishReload();
	void CancelReload();
	bool CheckCanWeaponReload();
	int32 GetAviableAmmoForReload();

	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector ShootDirection) const;
	void ChangeDispersionByShoot();

	int8 GetNumberProjectileByShot() const;

private:
	void FireWithProjectile(const FProjectileInfo& ProjectileInfo, const FVector& SpawnLocation,
		FRotator SpawnRotation, const FVector& EndLocation);
	
	void FireWithTrace(const FProjectileInfo& ProjectileInfo, const FVector& SpamLocation, const FVector& EndLocation);
};


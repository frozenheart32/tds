﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TestModel.h"

UTestModel::UTestModel()
{
	
}

void UTestModel::SetModelRepository(UModelRepository* ModelRepository)
{
	Super::SetModelRepository(ModelRepository);
}

void UTestModel::StartSession()
{
	Super::StartSession();
	//init, get models and To Subscribe on events
}

void UTestModel::IncrementScore(int32 AddScoreValue)
{
	Score += AddScoreValue;
	OnScoreChanged.Broadcast(Score);
}

int32 UTestModel::GetScore() const
{
	return Score;
}
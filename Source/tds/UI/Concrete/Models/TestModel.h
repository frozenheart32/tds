﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../../UI/MVVMLibrary/Abstract/SessionModel.h"
#include "TestModel.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnScoreChanged, int32, NewScoreValue);
/**
 * 
 */
UCLASS(Blueprintable)
class TDS_API UTestModel : public USessionModel
{
	GENERATED_BODY()
	
public:
	
	UTestModel();
	
	UPROPERTY(BlueprintAssignable)
	FOnScoreChanged OnScoreChanged;
	
private:

	int32 Score = 0;
	
public:

	
	virtual void SetModelRepository(UModelRepository* ModelRepository) override;
	virtual void StartSession() override;

	UFUNCTION(BlueprintCallable)
	void IncrementScore(int32 AddScoreValue);
	UFUNCTION(BlueprintCallable)
	int32 GetScore() const;
};

﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TestViewModel.h"

#include "tds/UI/Concrete/Views/TestView.h"
#include "Templates/SubclassOf.h"


UTestViewModel::UTestViewModel()
{
	
}

void UTestViewModel::SetModelRepository(UModelRepository* ModelRepository)
{
	Super::SetModelRepository(ModelRepository);
	TSubclassOf<USessionModel> TestModelType = UTestModel::StaticClass();
	
	TestModel = Cast<UTestModel>(Repository->GetModel(TestModelType));
}

void UTestViewModel::InitializeViewModel(UView* View)
{
	if(!View)
	{
		//UE_Log_Error
		return;
	}
	
	Super::InitializeViewModel(View);

	//Subscribe on View
	auto MyView = Cast<UTestView>(View);
	if(MyView)
	{
		TestView = MyView;
		MyView->AddScoreButtonClicked.AddDynamic(this, &UTestViewModel::OnButtonClicked);
	}
	else
	{
		//UE_LOG ERROR
	}

	//Subscribe on models
	TestModel->OnScoreChanged.AddDynamic(this, &UTestViewModel::OnScoreUpdate);
}

void UTestViewModel::OnButtonClicked()
{
	TestModel->IncrementScore(100);
}

void UTestViewModel::OnScoreUpdate(int32 NewScoreValue)
{
	FString String = FString::Printf(TEXT("Your score: %d"), NewScoreValue);
	FText Text = FText::FromString(String);
	TestView->UpdateScoreText(Text);
}
﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "tds/UI/Concrete/Models/TestModel.h"
#include "tds/UI/MVVMLibrary/Abstract/ViewModel.h"
#include "TestViewModel.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TDS_API UTestViewModel : public UViewModel
{
	GENERATED_BODY()
public:
	
	UTestViewModel();

private:
	
	UTestModel* TestModel = nullptr;
	UTestView* TestView = nullptr;
	
protected:
	UFUNCTION()
	void OnButtonClicked();

	UFUNCTION()
	void OnScoreUpdate(int32 NewScoreValue);
	
public:
	virtual void SetModelRepository(UModelRepository* ModelRepository) override;
	virtual void InitializeViewModel(UView* View) override;
};

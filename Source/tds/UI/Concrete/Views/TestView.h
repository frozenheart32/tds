﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "tds/UI/MVVMLibrary/Abstract/View.h"
#include "TestView.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUserButtonClicked);

UCLASS(Blueprintable)
class TDS_API UTestView : public UView
{
	GENERATED_BODY()

public:
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnUserButtonClicked AddScoreButtonClicked;

	UFUNCTION(BlueprintNativeEvent)
	void UpdateScoreText(const FText& Text);
};

﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "ModelRepository.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TDS_API UModelRepository : public UObject
{
	GENERATED_BODY()
public:
	UModelRepository();

private:

	TMap<UClass*, UObject*> ModelsDictionary;

	UObject* CreateModel(TSubclassOf<USessionModel> ModelType);
	
public:
	
	UFUNCTION(BlueprintCallable)
	UObject* GetModel(TSubclassOf<USessionModel> ModelType);
	UFUNCTION(BlueprintCallable)
	void CloseSession();
};

﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SessionModel.h"

USessionModel::USessionModel()
{
	
}

void USessionModel::SetModelRepository(UModelRepository* ModelRepository)
{
	Repository = ModelRepository;
}

void USessionModel::StartSession()
{
	
}

void USessionModel::EndSession()
{
	Repository = nullptr;
}

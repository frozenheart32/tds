﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "tds/UI/Concrete/Views/TestView.h"
#include "tds/UI/MVVMLibrary/ModelRepository.h"
#include "UObject/Object.h"
#include "ViewModel.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TDS_API UViewModel : public UObject
{
	GENERATED_BODY()
	
public:
	
	UViewModel();

protected:
	
	UModelRepository* Repository = nullptr;

public:
	
	virtual void SetModelRepository(UModelRepository* ModelRepository);
	virtual void InitializeViewModel(UView* View);
};

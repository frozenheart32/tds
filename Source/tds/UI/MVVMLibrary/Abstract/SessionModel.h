﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "tds/UI/MVVMLibrary/ModelRepository.h"
#include "SessionModel.generated.h"

// This class does not need to be modified.
UCLASS(Blueprintable)
class USessionModel : public UObject
{
	GENERATED_BODY()

public:
	
	USessionModel();
	
protected:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Models")
	UModelRepository* Repository = nullptr;

public:
	
	virtual void SetModelRepository(UModelRepository* ModelRepository);
	virtual void StartSession();
	virtual void EndSession();
};

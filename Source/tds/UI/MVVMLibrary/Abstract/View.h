﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "View.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TDS_API UView : public UUserWidget
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="ViewModelSettings")
	TSubclassOf<class UViewModel> ViewModelClassType;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="ViewModelSettings")
	UViewModel* ViewModel = nullptr;
	
public:

	UFUNCTION(BlueprintNativeEvent)
	void InitializeView(UModelRepository* ModelRepository); 
};

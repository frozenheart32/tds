﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "View.h"

#include "ViewModel.h"
#include "tds/UI/MVVMLibrary/ModelRepository.h"

void UView::InitializeView_Implementation(UModelRepository* ModelRepository)
{
	ViewModel = Cast<UViewModel>(ViewModelClassType.GetDefaultObject());
	ViewModel->SetModelRepository(ModelRepository);
	ViewModel->InitializeViewModel(this);
}

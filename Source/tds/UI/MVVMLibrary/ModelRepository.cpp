﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ModelRepository.h"
#include "Abstract/SessionModel.h"
#include "Templates/SubclassOf.h"


UModelRepository::UModelRepository()
{
	
}

UObject* UModelRepository::CreateModel(TSubclassOf<USessionModel> ModelType)
{
	auto NewModel = NewObject<UObject>(this, ModelType);
	ModelsDictionary.Add(ModelType, NewModel);
	
	USessionModel* NewSessionModel = Cast<USessionModel>(NewModel);
	if(NewSessionModel)
	{
		NewSessionModel->SetModelRepository(this);
		NewSessionModel->StartSession();
	}
	return NewModel;
}

UObject* UModelRepository::GetModel(TSubclassOf<USessionModel> ModelType)
{
	if(ModelsDictionary.Contains(ModelType))
	{
		return ModelsDictionary[ModelType];
	}

	return CreateModel(ModelType);
}

void UModelRepository::CloseSession()
{
	TArray<UClass*> Keys;
	ModelsDictionary.GetKeys(Keys);
	for (auto Key : Keys)
	{
		auto Model = Cast<USessionModel>(ModelsDictionary[Key]);
		if(Model)
		{
			Model->EndSession();
		}
	}
}

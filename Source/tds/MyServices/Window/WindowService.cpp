﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WindowService.h"

UView* UWindowService::CreateWindow(const TSubclassOf<UView> WindowType, UUserWidget* Parent)
{
	auto NameText = WindowType->GetName();
	if(Parent)
	{
		return Cast<UView>(CreateWidget(Parent, WindowType, FName{NameText}));
	}
	else
	{
		return Cast<UView>(CreateWidget(GetWorld(), WindowType, FName{NameText}));
	}
}

void UWindowService::Initialize(UModelRepository* Repository)
{
	if(!ModelRepository)
	{
		ModelRepository = Repository;
	}
}

UView* UWindowService::OpenWindow(TSubclassOf<UView> WindowType, bool& Result, UUserWidget* Parent)
{
	Result = false;
	if(OpenWindows.Contains(WindowType))
	{
		return  OpenWindows[WindowType];
	}
	
	auto Window = CreateWindow(WindowType, Parent);
	if(Window)
	{
		Window->AddToViewport();
		Window->InitializeView(ModelRepository);
		OpenWindows.Add(WindowType, Window);
		Result = true;
	}
	return Window;
}

bool UWindowService::CloseWindow(TSubclassOf<UView> WindowType)
{
	if(OpenWindows.Contains(WindowType))
	{
		auto Window = OpenWindows[WindowType];
		OpenWindows.Remove(WindowType);
		Window->RemoveFromParent();
		return true;
	}

	return false;
}

void UWindowService::CloseAllWindows()
{
	TArray<UClass*> Keys;
	OpenWindows.GetKeys(Keys);
	for (const auto Key : Keys)
	{
		auto Window = OpenWindows[Key];
		OpenWindows.Remove(Key);
		Window->RemoveFromParent();
	} 
}

bool UWindowService::IsOpen(TSubclassOf<UView> WindowType) const
{
	return OpenWindows.Contains(WindowType);
}

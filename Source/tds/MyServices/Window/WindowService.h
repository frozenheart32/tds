﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "tds/UI/MVVMLibrary/Abstract/View.h"
#include "UObject/Object.h"
#include "WindowService.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TDS_API UWindowService : public UObject
{
	GENERATED_BODY()

private:

	TMap<UClass*, UView*> OpenWindows;
	UModelRepository* ModelRepository = nullptr;

	UView* CreateWindow(const TSubclassOf<UView> WindowType, UUserWidget* Parent);
	
public:

	void Initialize(UModelRepository* Repository);
	
	UFUNCTION(BlueprintCallable, Category="Window Service")
	UView* OpenWindow(TSubclassOf<UView> WindowType, bool& Result, UUserWidget* Parent = nullptr);
	UFUNCTION(BlueprintCallable, Category="Window Service")
	bool CloseWindow(TSubclassOf<UView> WindowType);
	UFUNCTION(BlueprintCallable, Category="Window Service")
	void CloseAllWindows();
	UFUNCTION(BlueprintCallable, Category="Window Service")
	bool IsOpen(TSubclassOf<UView> WindowType) const;
};

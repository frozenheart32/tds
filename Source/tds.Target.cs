// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class tdsTarget : TargetRules
{
	public tdsTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("tds");
	}
}
